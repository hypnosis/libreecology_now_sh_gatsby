import * as React from 'react'
import { Link } from 'gatsby'
import { Helmet } from 'react-helmet';

import { 
  websiteCredits, 
  footerBlocks,
  navLinks,
  navLinkItem,
  navLinkText 
} from '../styles/index.css'


const Layout = ({ pageTitle, children }) => {
  return (
    <main>
      <Helmet>
        <title>LibreEcology.org | {pageTitle}</title>
      </Helmet>
      <nav>
        <ul>
          <li style={{paddingRight: "8px", display: "inline list-item"}}><Link to="/">Home</Link></li>
          <li style={{paddingRight: "8px", display: "inline list-item"}}><Link to="/development">Development</Link></li>
        </ul>
      </nav>
      <h1>LibreEcology.org &gt; {pageTitle}</h1>
      {children}
      <p className={websiteCredits}><a href="https://gitlab.com/hypnosis/libreecology_now_sh_gatsby" target="_blank" rel="noreferrer noopener">This website&#39;s source code</a> is hosted by{' '} <a href="https://gitlab.com/" target="_blank" rel="noreferrer noopener">GitLab</a>. Deployed with{' '} <a href="https://vercel.com/docs" target="_blank" rel="noreferrer noopener">Vercel</a>. Powered by{' '} <a href="https://www.gatsbyjs.org/" target="_blank" rel="noreferrer noopener">Gatsby</a>.</p> 
      <p><em>Copyright &copy; 2021 LibreEcology.org. All Rights Reserved.</em></p>
    </main>
  )
}

export default Layout
