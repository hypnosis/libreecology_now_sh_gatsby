import * as React from 'react'
import Layout from '../components/layout'
/*
import React, { useEffect, useState } from 'react';
import { Link } from 'gatsby'
import '../styles/index.css';
*/
const IndexPage = () => {
  return (
    <Layout pageTitle="Home Page">
      <p>You&apos;re welcome. This is a work in progress.</p>
    </Layout>
  )
}

export default IndexPage
/*
function Index() {
  const [date, setDate] = useState(null);
  useEffect(() => {
    async function getDate() {
      const res = await fetch('/api/date');
      const newDate = await res.text();
      setDate(newDate);
    }
    getDate();
  }, []);
  return (
    <main>
      <Helmet>
        <title>LibreEcology.org</title>
      </Helmet>
      <h1><a href="/">LibreEcology.org</a></h1>
      <hr/>
      <nav>
        <ul className={navLinks}>
          <li className={navLinkItem} style="display:inline list-item;padding: 8px;">
            <Link to="/" className={navLinkText}>
              Home
            </Link>
          </li>
          <li className={navLinkItem} style="display:inline list-item;padding: 8px;">
            <Link to="/development" className={navLinkText}>
              Development
            </Link>
          </li>
        </ul>
      </nav>
      <hr/>
    </main>
  );
}

export default Index;
*/
