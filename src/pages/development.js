import * as React from 'react'
import Layout from '../components/layout'
/*
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'gatsby'
import '../styles/index.css';
*/
/*
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'gatsby'
*/
import '../styles/index.css';
import { 
  websiteCredits, 
  footerBlocks,
  navLinks,
  navLinkItem,
  navLinkText 
} from '../styles/index.css'

const DevelopmentPage = () => {
  return (
    <Layout pageTitle="Development Page">
      <p>You&apos;re welcome. This is a work in progress.</p>
    </Layout>
  )
}

export default DevelopmentPage

/*
function Development() {
  const [date, setDate] = useState(null);
  useEffect(() => {
    async function getDate() {
      const res = await fetch('/api/date');
      const newDate = await res.text();
      setDate(newDate);
    }
    getDate();
  }, []);
  return (
    <main>
      <Helmet>
        <title>LibreEcology.org</title>
      </Helmet>
      <h1><a href="/">LibreEcology.org</a></h1>
      <p>
        <a
          href="https://github.com/vercel/vercel/tree/main/examples/gatsby"
          target="_blank"
          rel="noreferrer noopener"
        >
          This project
        </a>{' '}
        is a modified <a href="https://www.gatsbyjs.org/">Gatsby</a> example app with two
        directories, <code>/src</code> for static content and <code>/api</code>{' '}
        which contains a serverless{' '}
        <a href="https://nodejs.org/en/">Node.js (TypeScript)</a> function. See{' '}
        <a href="/api/date">
          <code>api/date</code> for the Date API with Node.js (TypeScript)
        </a>
        .
      </p>
      <br />
      <h2>The date according to Node.js (TypeScript) is:</h2>
      <p>{date ? date : 'Loading date...'}</p>
      <hr/>
      <nav>
        <ul className={navLinks}>
          <li className={navLinkItem} style="display:inline list-item;padding: 8px;">
            <Link to="/" className={navLinkText}>
              Home
            </Link>
          </li>
          <li className={navLinkItem} style="display:inline list-item;padding: 8px;">
            <Link to="/development" className={navLinkText}>
              Development
            </Link>
          </li>
        </ul>
      </nav>
      <hr/>
      <p className={websiteCredits}><a href="https://gitlab.com/hypnosis/libreecology_now_sh_gatsby" target="_blank" rel="noreferrer noopener">This website&#39;s source code</a> is hosted by{' '} <a href="https://gitlab.com/" target="_blank" rel="noreferrer noopener">GitLab</a>. Deployed with{' '} <a href="https://vercel.com/docs" target="_blank" rel="noreferrer noopener">Vercel</a>. Powered by{' '} <a href="https://www.gatsbyjs.org/" target="_blank" rel="noreferrer noopener">Gatsby</a>.</p> 
      <p><em>Copyright &copy; 2021 LibreEcology.org. All Rights Reserved.</em></p>
    </main>
  );
}

export default Development;
*/
